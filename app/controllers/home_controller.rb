# frozen_string_literal: true

# Signle controller
class HomeController < ApplicationController
  # GET /index
  def index; end

  # GET /sitemap
  def sitemap; end

  # GET /cgu
  def cgu; end

  # GET /contact
  def contact; end

  # GET /privacy
  def privacy; end
end
