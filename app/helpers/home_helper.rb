# frozen_string_literal: true

# Helper for home pages
module HomeHelper
  def timeline(year, month, last: false)
    tag.div id: id_timeline(year, month), class: 'timeline-item' do
      capture do
        concat timeline_left(year, month, last)
        concat timeline_right(year, month)
      end
    end
  end

  private

  def id_timeline(year, month)
    "timeline-#{year}-#{month}"
  end

  def timeline_left(year, month, last)
    tag.div class: 'timeline-left' do
      tag.a class: timeline_icon(last), href: "##{id_timeline(year, month)}" do
        tag.i class: 'icon icon-check' unless last
      end
    end
  end

  def timeline_icon(last)
    if last
      'timeline-icon'
    else
      'timeline-icon icon-lg'
    end
  end

  def timeline_right(year, month)
    tag.div class: 'timeline-content' do
      capture do
        concat timeline_right_title(year, month)
        concat timeline_right_desc(year, month)
      end
    end
  end

  def timeline_right_title(year, month)
    tag.div class: 'title' do
      tag.h3 I18n.t("timeline.#{year}.#{month}.title")
    end
  end

  def timeline_right_desc(year, month)
    tag.div class: 'content' do
      tag.p I18n.t("timeline.#{year}.#{month}.desc")
    end
  end
end
