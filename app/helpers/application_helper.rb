# frozen_string_literal: true

# Generic method helper
module ApplicationHelper
  def title_page
    tag.div class: 'columns' do
      tag.div class: 'column col-12' do
        tag.h1 { title }
      end
    end
  end

  def title
    I18n.t("titles.#{controller_name}.#{action_name}")
  end

  def images_count(path)
    1..array_images(path).count
  end

  def image_carousel(path, indx)
    "#{path}/#{array_images(path)[indx - 1]}"
  end

  private

  def path_images(path)
    Dir[Rails.root.join('app', 'assets', 'images', path)].join('/')
  end

  def array_images(path)
    Dir.entries(path_images(path)) - ['..', '.']
  end
end
