# frozen_string_literal: true

require File.expand_path('../config/environment', __dir__)
require 'rails/test_help'

module ActiveSupport
  class TestCase
    # Add more helper methods to be used by all tests here...
  end
end
