# frozen_string_literal: true

Rails.application.routes.draw do
  # Home path -- index
  root to: 'home#index'

  # Static page
  get '/index',     to: 'home#index'
  get '/sitemap',   to: 'home#sitemap'
  get '/cgu',       to: 'home#cgu'
  get '/privacy',   to: 'home#privacy'
  get '/contact',   to: 'home#contact'
end
