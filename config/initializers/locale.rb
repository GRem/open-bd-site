# frozen_string_literal: true

# Loading locales
I18n.load_path += Dir[Rails.root.join('config', 'locales', '*.yml')]

# Whitelist locales available for the application
I18n.available_locales = [:fr]

# Set default locale to something other than :en
I18n.default_locale = :fr
