# frozen_string_literal: true

# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = if ENV.key?('APP_WEB_HOST')
                                           "http://#{ENV['APP_WEB_HOST']}"
                                         else
                                           'http://openbd.local'
                                         end

SitemapGenerator::Sitemap.create do
  add '/index',   changefreq: :monthly
  add '/about',   changefreq: :never
  add '/sitemap', changefreq: :never
  add '/cgu',     changefreq: :never
  add '/contact', changefreq: :never
end
