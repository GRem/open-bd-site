# frozen_string_literal: true

source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

gem 'config', '~> 3.1', '>= 3.1.1'
gem 'haml', '~> 5.2', '>= 5.2.2'
gem 'mini_portile2', '~> 2.7', '>= 2.7.1'
gem 'puma', '~> 5.5', '>= 5.5.2'
gem 'rack-http-accept-language', '~> 0.1.1'
gem 'rails', '~> 7.0', '>= 7.0.1'
gem 'sass-rails', '~> 6.0'
gem 'sitemap_generator', '~> 6.2', '>= 6.2.1'
gem 'spectre_scss', '~> 0.5.9.0'

group :local do
  gem 'brakeman', '~> 5.2'
  gem 'bundle-audit', '~> 0.1.0'
  gem 'haml_lint', '~> 0.37.0'
  gem 'pry-rails', '~> 0.3.9'
  gem 'rubocop', '~> 1.25'
  gem 'scss_lint', '~> 0.59.0'
end

gem 'tzinfo-data', '~> 1.2021', '>= 1.2021.5', platforms: %i[mingw mswin x64_mingw jruby]
